// linklist.cpp : Defines the entry point for the console application.
//

#include <iostream>

using namespace std;

struct inode {
	int data;
	inode *next;
};

bool isEmpty(inode *root)
{
	return (root == NULL);
}

char menu()
{
	char option;

	cout << "Options: \n";
	cout << "1. Add an item to top.\n";
	cout << "2. Add an item to bottom.\n";
	cout << "3. Remove an item from top.\n";
	cout << "4. Remove an item from bottom.\n";
	cout << "5. Show the list.\n";
	cout << "6. Quit\n";

	cin >> option;

	return option;
}

void firstNode(inode *&root, inode *&tail, int number)
{
	inode *temp = new inode;
	temp->data = number;
	temp->next = NULL;
	root = temp;
	tail = temp;
}

void prependNode(inode *&root, inode *&tail, int number)
{
	if (isEmpty(root))
	{
		firstNode(root, tail, number);
	}
	else
	{
		inode *temp = new inode;
		temp->data = number;
		temp->next = root;
		root = temp;

	}
}

void appendNode(inode *&root, inode *&tail, int number)
{
	if (isEmpty(root))
	{
		firstNode(root, tail, number);
	}
	else
	{
		inode *temp = new inode;
		temp->data = number;
		temp->next = NULL;
		tail->next = temp;
		tail = temp;
	}
}

void removeFromRoot(inode *&root, inode *&tail)
{
	if (isEmpty(root))
	{
		cout << "List is empty\n";
	}
	else if (root == tail)
	{
		delete root;
		root = NULL;
		tail = NULL;
	}
	else
	{
		inode *temp = root;
		root = root->next;
		delete temp;
	}

}

void removeFromTail(inode *&root, inode *&tail)
{
	if (isEmpty(root))
	{
		cout << "List is empty\n";
	}
	else if (root == tail)
	{
		delete root;
		root = NULL;
		tail = NULL;
	}
	else
	{
		inode *temp = root;
		inode *prev = temp;
		while (temp != tail)
		{
			prev = temp;
			temp = temp->next;
		} 

		delete temp;
		prev->next = NULL;
		tail = prev;
	}

}
void showList(inode *current) 
{
	if (isEmpty(current))
	{ 
		cout << "List is empty\n";
	}
	else
	{
		cout << "List contains: \n";
		while (current != NULL)
		{
			cout << current->data << endl;
			current = current->next;
		}
	}
}

int main()
{
	inode *root = NULL;
	inode *tail = NULL;

	char option = 'x';
	int number;

	while (option != '6')
	{
		option = menu();
		switch (option)
		{
		case '1':
			cout << "Enter a number: ";
			cin >> number;
			prependNode(root, tail, number);
			break;
		case '2':
			cout << "Enter a number: ";
			cin >> number;
			appendNode(root, tail, number);
			break;
		case '3':
			removeFromRoot(root, tail);
			break;
		case '4':
			removeFromTail(root, tail);
			break;
		case '5':
			showList(root);
			break;
		case '6':
			cout << "Quitting...\n";
		default:
			cout << "Chosen option not found\n";
		}
	}

    return 0;
}


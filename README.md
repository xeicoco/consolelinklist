# ConsoleLinkList
Sample link list C++ code with some actions:

```
	cout << "Options: \n";
	cout << "1. Add an item to top.\n";
	cout << "2. Add an item to bottom.\n";
	cout << "3. Remove an item from top.\n";
	cout << "4. Remove an item from bottom.\n";
	cout << "5. Show the list.\n";
	cout << "6. Quit\n";
```	